var _HEADER = {};
var _SIDEMENU = {};
var _MODALS = [];
var _SELECTS = [];
var _URL = new URL(window.location.href);

jQuery(function ($) {
    $(document).ready(function () {
        setSvgLoad(true);

        setMainBlocks();
        setModals();
        setSelects();
        setSliders();
        setAccordeons();
        setAnchors();
        setCopyInputs();
        setInputMask();
        setTimers();
    });


    function setInputMask() {
        let inputs_phone = $('input[mask="phone"]');
        let inputs_birth = $('input[mask="birth"]');
        let inputs_email = $('input[mask="email"]');

        setPhoneMask()
        setBirthMask()
        setEmailMask()

        function setPhoneMask() {
            inputs_phone.each((k, v) => {
                let input = $(v);

                input.inputmask({
                    mask: "+7 (099) 999-99-99",
                    definitions: {
                        "0": {
                            validator: "[1-9]",
                        }
                    }
                });
            })
        }
        function setBirthMask() {
            inputs_birth.each((k, v) => {
                let input = $(v);

                input.inputmask({
                    mask: "09.12.a999",
                    definitions: {
                        "0": {
                            validator: "[0-3]",
                        },
                        "1": {
                            validator: "[0-1]",
                        },
                        "2": {
                            validator: "[1-9]",
                        },
                        "a": {
                            validator: "[1-2]",
                        },
                    }
                });
            })
        }
        function setEmailMask() {
            inputs_email.each((k, v) => {
                let input = $(v);

                input.inputmask({
                    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
                    greedy: false,
                    // onBeforePaste: function (pastedValue, opts) {
                    //     pastedValue = pastedValue.toLowerCase();
                    //     return pastedValue.replace("mailto:", "");
                    // },
                    definitions: {
                        '*': {
                            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                            casing: "lower"
                        }
                    }
                });
            });
        }
    }

    function setTimers() {
        let timers = $('.__timer');
        timers.each((k, v) => {
            let timer = $(v);
            let targetDateStr = timer.attr('data-target-date');
            let targetDate = targetDateStr.toDate('hh:ii:ss dd.mm.yyyy');
            let isIntervalActive = true;
            let interval;
            let targetStr = false;

            let objects = {
                days: timer.find('.__days').find('.timer__title_key'),
                days_text: timer.find('.__days').find('.timer__title_value'),
                hours: timer.find('.__hours').find('.timer__title_key'),
                hours_text: timer.find('.__hours').find('.timer__title_value'),
                mins: timer.find('.__mins').find('.timer__title_key'),
                mins_text: timer.find('.__mins').find('.timer__title_value'),
                secs: timer.find('.__secs').find('.timer__title_key'),
                secs_text: timer.find('.__secs').find('.timer__title_value'),
            }

            // objects.days.css('min-width', objects.days.html('00').outerWidth(true));
            // objects.hours.css('min-width', objects.hours.html('00').outerWidth(true));
            objects.mins.css('min-width', objects.mins.html('00').outerWidth(true));
            objects.secs.css('min-width', objects.secs.html('00').outerWidth(true));

            targetStr = getCurDiff();

            interval = setInterval(() => {
                if (isIntervalActive) {
                    targetStr = getCurDiff()
                    setDiff();
                } else {
                    clearInterval(interval);
                    // timer.addClass('__ended')
                }
            }, 1000)

            function getCurDiff() {
                let now = new Date();
                let diff = (targetDate - now) / 1000;
                if (diff < 0) {
                    isIntervalActive = false;
                    return false
                }
                let targetStr = {
                    days: Math.floor(diff / 60 / 60 / 24).toString().padStart(2, '0'),
                    hours: Math.floor(diff / 60 / 60 % 24).toString().padStart(2, '0'),
                    mins: Math.floor(diff / 60 % 60).toString().padStart(2, '0'),
                    secs: Math.floor(diff % 60).toString().padStart(2, '0'),
                }

                targetStr.days_text = sklonenie(targetStr.days, ['день', 'дня', 'дней'])
                targetStr.hours_text = sklonenie(targetStr.hours, ['час', 'часа', 'часов'])
                targetStr.mins_text = sklonenie(targetStr.mins, ['минута', 'минуты', 'минут'])
                targetStr.secs_text = sklonenie(targetStr.secs, ['секунда', 'секунды', 'секунд'])

                return targetStr
            }
            function setDiff() {
                if (!targetStr) { return }
                Object.keys(objects).map(key => {
                    let box = objects[key];
                    let diff = targetStr[key];
                    box.html(diff);
                })
            }
        })
    }

    function setCopyInputs() {
        let textBoxes = $('.textBox.__copy');

        textBoxes.each((k, v) => {
            let textBox = $(v);
            let input = textBox.find('input');

            input.on('click', e => {
                navigator.clipboard.writeText(input.val()).then(() => {
                    textBox.addClass('__copied');
                    setTimeout(() => {
                        textBox.removeClass('__copied');
                    }, 1000);
                });

            });
        });
    }

    function setAnchors() {
        let anchors = $('.__anchor');

        anchors.each((k, v) => {
            let anchor = $(v);
            let target = $(anchor.attr('data-target'));

            if (!target.length) {
                return
            }

            let offset = target.offset().top - 24

            anchor.on('click', e => {
                e.preventDefault();
                $('html, body').stop().animate({ scrollTop: offset }, 600);
            });
        });
    }

    function setAccordeons() {
        let accordeons = $('.__accordeon');
        accordeons.each((k, v) => {
            let accordeon = $(v);
            let boxes = accordeon.find('.__accordeon__box');

            boxes.each((_k, _v) => {
                let box = $(_v);
                let head = box.find('.__accordeon__box_head');
                let body = box.find('.__accordeon__box_body');
                let isActive = false;
                let h = body.outerHeight();

                head.on('click', e => {
                    if (!isActive) {
                        isActive = true;
                        box.addClass('__active');
                    } else {
                        isActive = false;
                        box.removeClass('__active');
                    }
                });

                body.css({ height: h + 'px' });

                box.addClass('__inited');
            });
        });
    }

    function setSelects() {
        let selects = $('select');

        selects.each((k, v) => {
            let select = $(v);
            let selectObj = new Select(select);
            _SELECTS.push(selectObj);
        });
        $(window).trigger('__selects_ready');
    }

    function setModals() {
        let modals = $('.__modal');

        modals.each((k, v) => {
            let modal = $(v);
            let modalObj = new Modal(modal);
            _MODALS.push(modalObj);

            console.log('shlap');
        });
        $(window).trigger('__modals_ready');
    }

    function setSliders() {
        let sliders = $('.__slider');
        sliders.each((k, v) => {
            let slider = $(v);
            let parent = slider.closest('.__slider_parent');
            let wrapper = slider.find('.swiper-wrapper');
            let swiper

            let withPager = slider.find('.swiper-pagination').length ? true : false;

            if (withPager) {
                parent.addClass('__with_pager');
            }

            swiper = new Swiper(slider[0], {
                draggable: true,
                slidesPerView: "auto",
                // loop: true,
                // centeredSlides: true,
                // speed: 3000,
                slideToClickedSlide: slider.hasClass('__click'),
                // rewind: true,
                // autoHeight: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    type: "bullets"
                },
                navigation: {
                    nextEl: parent.find(".swiper-button-next")[0],
                    prevEl: parent.find(".swiper-button-prev")[0],
                },

            });
        });
    }

    function setMainBlocks() {
        _HEADER = new Header();
        _ASIDE = new Sidemenu();
    }

});

class Sidemenu {
    aside;
    burger;
    box;
    isActive;

    constructor() {
        this.aside = $('.sideMenu');
        this.outer = this.aside.find('.sideMenu__outer');
        this.burger = $('#__burgerBox');
        this.shadow = this.aside.find('.sideMenu__shadow');
        this.isActive = false;

        this.setEventListeners();
    }

    setTransitions = (type) => {
        this.items = this.aside.find('._menu a');

        let i = 0;
        let d = 0
        this.items.each((k, v) => {
            let item = $(v);
            if (i !== 0) {
                let delay = 0.04 * i + 0.05;
                if (type) {
                    item.css('transition-delay', delay + 's');
                } else {
                    item.css('transition-delay', '0s');
                }
                d = delay;
            }
            i++;
        });
        if (type) {
            setTimeout(() => {
                this.items.each((k, v) => {
                    let item = $(v);
                    if (i !== 0) {
                        item.css('transition-delay', '0s');
                    }
                    i++;
                });
            }, d * 1000);
        }
    }

    setEventListeners = () => {
        this.burger.on('click', this.toggle);
        this.shadow.on('click', this.hide);

        $(window).on('resize', e => {
            if (window.innerWidth > 768) {
                if (this.isActive) {
                    this.hide();
                }
            }
        });
    }

    show = () => {
        this.setTransitions(true);
        this.isActive = true;
        this.aside.addClass('__active');
        this.burger.addClass('__active');

        _HEADER.setLightTheme(false);
        $('body').addClass('__menuOpened');
        fixWindow(true);
    }
    hide = () => {
        this.setTransitions(false);
        this.isActive = false;
        this.aside.removeClass('__active');
        this.burger.removeClass('__active');

        $('body').removeClass('__menuOpened');
        _HEADER.checkPos();
        fixWindow(false);
    }
    toggle = () => {
        if (this.isActive) {
            this.hide();
        } else {
            this.show();
        }
    }
}

class Select {
    isActive = false;
    box;
    head;
    body;
    options;
    select;
    name;
    opts = [];
    constructor(obj) {
        this.select = obj;
        this.id = this.select.attr('id');
        this.withSearch = this.select.attr('search') !== undefined ? true : false;

        this.build();
        console.log(this.withSearch);
    }
    build = () => {
        this.options = this.select.find('option');
        this.name = this.select.attr('name');

        this.box = $('<div class="__select" />');
        this.head = $('<div class="__select_head" />');
        this.headText = $('<div class="__select_head_text" />');
        this.body = $('<div class="__select_body" />');
        this.scroller = $('<div class="__select_scroller" />');
        this.option = $('<div class="__select_option" />');

        if (this.withSearch) {
            this.inputBox = $('<div class="__textBox" />');
            this.input = $('<input type="text" />');
            this.head.append(this.inputBox.append(this.input));
            this.head.addClass('__withSearch');

            this.input.on('input', e => {
                this.inputValue = this.input.val();
                this.checkInputValue();
            });
        }

        let acc = 0;
        this.options.each((k, v) => {
            let option = $(v);
            let value = option.attr('value');

            let opt = this.option.clone();
            opt.html(option.html());

            this.scroller.append(opt);

            this.opts.push({ opt, value: opt.html() });

            opt.on('click', e => {
                this.set(value);
            });
            if (acc === 0) {
                this.set(value);
            }
            if (option.attr('selected')) {
                this.set(value);
            }
            acc++;
        });
        // console.log(this.select.parent());
        // this.select.parent().append(this.box);
        this.select.parent().prepend(this.box);
        this.box.append(this.select);
        this.box.append(this.head);
        this.box.append(this.body);
        this.head.append(this.headText);
        this.body.append(this.scroller);

        this.box.attr('data-name', this.name);

        this.head.on('click', e => {
            if (!this.isActive) {
                this.show();
            } else {
                this.hide();
            }
        });

        $(document).on('click', e => {
            if (!$(e.target).closest('.__select.__active[data-name="' + this.name + '"]').length) {
                this.hide();
            }
        });
    }
    show = () => {
        this.isActive = true;
        this.box.addClass('__active');
        this.box.css('z-index', 20);
    }
    hide = () => {
        if (this.isActive) {
            this.box.css('z-index', 15);
            setTimeout(() => {
                this.box.css('z-index', 10);
            }, 210);
        }
        this.isActive = false;
        this.box.removeClass('__active');
    }
    set = (value, type = true) => {
        this.options.each((k, v) => {
            let option = $(v);
            let val = option.attr('value');
            if (val === value) {
                if (this.withSearch) {
                    this.input.val(option.html());
                } else {
                    this.headText.html(option.html());
                }
            }
        });
        this.select.val(value);
        this.value = value;
        this.hide();
        if (type) {
            this.onSet();
        }
        console.log('set', this.value);
    }
    checkInputValue = () => {
        this.show();
        this.opts.map(el => {
            if (this.inputValue.length > 0) {
                if (el.value.toLowerCase().includes(this.inputValue.toLowerCase())) {
                    el.opt.show();
                } else {
                    el.opt.hide();
                }
            } else {
                el.opt.show();
            }
        });
    }
    onSet = () => {
        console.log('onSet');

    }
}

class Header {
    header;
    isFilled = false;

    constructor() {
        this.header = $('header');

        this.setEventListeners();
    }

    setEventListeners = () => {
        $(window).on('scroll', e => {
            let scrollTop = $(window).scrollTop();
            if (scrollTop > this.header.outerHeight()) {
                this.setFilled(false);
            } else {
                this.setFilled(true);
            }
        });
    }

    fill = () => {
        this.isFilled = true
        this.header.addClass('__filled');
    }
    unFill = () => {
        this.isFilled = false
        this.header.removeClass('__filled');
    }
    setFilled = (type) => {
        if (type) {
            if (this.isFilled) {
                this.unFill();
            }
        } else {
            if (!this.isFilled) {
                this.fill();
            }
        }
    }
}


class Modal {
    name;
    obj;
    shadow;
    close;
    isActive;

    constructor(obj) {
        this.obj = obj;
        this.name = this.obj.attr('data-name');
        this.close = this.obj.find('.__modal_close');
        this.shadow = this.obj.find('.__modal_shadow');
        this.closeEl = this.obj.find('.__modal_close_el');
        this.inner = this.obj.find('.__modal_inner');

        this.build();
    }

    build = () => {
        this.close.append('<span />').append('<span />');
        this.close.on('click', () => { if (this.canClose) { this.hide() } });
        this.closeEl.on('click', this.hide);
        this.shadow.on('click', () => { if (this.canClose) { this.hide() } });

        $(document).on('click', '.__call_modal[data-modal-name="' + this.name + '"]', e => {
            e.preventDefault();
            this.show();
        });

        this.setClose(true);

        // this.target.on('click', e => { e.preventDefault(); this.show() });
        setTimeout(() => {
            if (this.obj.hasClass('__active')) {
                this.show();
            }
            if (this.obj.hasClass('__noClose')) {
                this.setClose(false);
            }
        }, 500);

    }

    setClose = type => {
        this.canClose = type
    }

    show = () => {
        _MODALS.map(el => {
            el.hide();
        });

        _HEADER.setVisibilty(false);
        _ASIDE.hide();

        this.setIndexZ(true);
        this.isActive = true;
        this.obj.addClass('__visible');
        setTimeout(() => {
            this.obj.addClass('__active');
        }, 10);
        fixWindow(true);

    }

    setIndexZ = (type) => {
        if (type) {
            this.obj.addClass('__z_top');
        } else {
            this.obj.removeClass('__z_top');
        }
    }

    onHide = () => {

    }

    hide = () => {
        this.setIndexZ(false);
        this.isActive = false;
        this.obj.removeClass('__active');
        _HEADER.setVisibilty(true);
        setTimeout(() => {
            if (!this.isActive) {
                this.obj.removeClass('__visible');
            }
        }, 400);

        fixWindow(false);
        this.onHide();
    }
}

function getModal(name) {
    return _MODALS.filter(el => el.name === name)[0];
}

function getSelect(name) {
    return _SELECTS.filter(el => el.id === name)[0];
}

function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setWordEnding(number, txt, cases = [2, 0, 1, 1, 1, 2]) {
    return txt[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
};

function setSvgLoad(type = false) {
    let svgs = $('.__svg_load');
    let count = 0;
    svgs.each((k, v) => {
        let svg = $(v);

        if (svg.hasClass('__loaded')) {
            return;
        }

        let img = svg.find('img');
        let attr = img.attr('src');

        $.ajax({
            url: attr,
            success: res => {
                svg.html($(res).find('svg'));
                svg.addClass('__loaded');
                if (type) {
                    console.log('load svg', count, svgs.length);
                    count++;
                    if (count === svgs.length) {
                        $(window).trigger('svg_loaded');
                    }
                }

            },
            error: err => {
                console.log(err);
            }
        });
    });
}

function fixWindow(type) {
    if (type) {
        setTimeout(function () {
            /* Ставим необходимую задержку, чтобы не было «конфликта» в случае, если функция фиксации вызывается сразу после расфиксации (расфиксация отменяет действия расфиксации из-за одновременного действия) */
            if (!document.body.hasAttribute('data-body-scroll-fix')) {
                // Получаем позицию прокрутки
                let scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
                // Ставим нужные стили
                document.body.setAttribute('data-body-scroll-fix', scrollPosition); // Cтавим атрибут со значением прокрутки
                document.body.style.overflow = 'hidden';
                document.body.style.position = 'fixed';
                document.body.style.top = '-' + scrollPosition + 'px';
                document.body.style.left = '0';
                document.body.style.width = '100%';
            }
        }, 15);
    } else {
        if (document.body.hasAttribute('data-body-scroll-fix')) {
            // Получаем позицию прокрутки из атрибута
            let scrollPosition = document.body.getAttribute('data-body-scroll-fix');
            // Удаляем атрибут
            document.body.removeAttribute('data-body-scroll-fix');
            // Удаляем ненужные стили
            document.body.style.overflow = '';
            document.body.style.position = '';
            document.body.style.top = '';
            document.body.style.left = '';
            document.body.style.width = '';
            // Прокручиваем страницу на полученное из атрибута значение
            window.scroll(0, scrollPosition);
        }
    }
}

String.prototype.toDate = function (format) {
    var normalized = this.replace(/[^a-zA-Z0-9]/g, '-');
    var normalizedFormat = format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
    var formatItems = normalizedFormat.split('-');
    var dateItems = normalized.split('-');

    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var hourIndex = formatItems.indexOf("hh");
    var minutesIndex = formatItems.indexOf("ii");
    var secondsIndex = formatItems.indexOf("ss");

    var today = new Date();

    var year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
    var month = monthIndex > -1 ? dateItems[monthIndex] - 1 : today.getMonth() - 1;
    var day = dayIndex > -1 ? dateItems[dayIndex] : today.getDate();

    var hour = hourIndex > -1 ? dateItems[hourIndex] : today.getHours();
    var minute = minutesIndex > -1 ? dateItems[minutesIndex] : today.getMinutes();
    var second = secondsIndex > -1 ? dateItems[secondsIndex] : today.getSeconds();

    return new Date(year, month, day, hour, minute, second);
};
