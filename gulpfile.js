const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const debug = require('gulp-debug');
const del = require('del');

const templatePath = './www/local/templates/konkurs2022';
const paths = {
    styles: {
        src: [
            templatePath + '/css/*.scss',
        ],
        dest: templatePath + '/public/'
    },
    scripts: {
        src: [
            templatePath + '/js/*.js',
            // templatePath + '/components/**/*.js',
        ],
        dest: templatePath + '/public/'
    },
    scriptsES5: {
        src: [
            templatePath + '/js/*.js',
            // templatePath + '/components/**/*.js',
        ],
        dest: templatePath + '/public/'
    },
    componentScripts: {
        src: [
            // templatePath + '/components/script.js',
            // templatePath + '/components/**/script.js',
            // templatePath + '/components/bitrix/system.auth.form/\.modal/script.js',
            // templatePath + '/components/*/*/script.js',
        ]
    },
    libs: {
        src: [
            templatePath + '/libs/jquery.min.js',
            // templatePath + '/libs/swiper.min.js',
        ],
        dest: templatePath + '/public/'
    },
    libsES5: {
        src: [
            templatePath + '/libs/jquery.min.js',
            // templatePath + '/libs/slick.min.js',
        ],
        dest: templatePath + '/public/'
    }
};

function clean() {
    return del([templatePath + '/public']);
}


function cssDev() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest(paths.styles.dest));
}
function cssProd() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest(paths.styles.dest));
}

function jsDev() {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(babel())
        // .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
}
function jsDevES5() {
    return gulp.src(paths.scriptsES5.src, { sourcemaps: true })
        .pipe(babel({
            "presets": ["@babel/preset-env"]
        }))
        .pipe(concat('main.min.es5.js'))
        .pipe(gulp.dest(paths.scriptsES5.dest));
}
function jsComponents() {
    return gulp.src(paths.componentScripts.src, { sourcemaps: true, base: "./" })
        .pipe(babel({
            "presets": ["@babel/preset-env"]
        }))
        .pipe(uglify())
        .pipe(rename({ suffix: '.es5' }))
        .pipe(gulp.dest('.'));
}
function jsProd() {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(babel({
            "presets": ["@babel/preset-env"]
        }))
        .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
}
function libs() {
    return gulp.src(paths.libs.src, { sourcemaps: true })
        .pipe(babel())
        .pipe(uglify())
        .pipe(concat('libs.min.js'))
        .pipe(gulp.dest(paths.libs.dest));
}
function libsES5() {
    return gulp.src(paths.libsES5.src, { sourcemaps: true })
        .pipe(babel({
            "presets": ["@babel/preset-env"]
        }))
        .pipe(uglify())
        .pipe(concat('libs.min.es5.js'))
        .pipe(gulp.dest(paths.libsES5.dest));
}

function watchDev() {
    gulp.series(clean, gulp.parallel(jsDev, jsDevES5, cssDev, libs, libsES5));

    gulp.watch(paths.scripts.src, jsDev);
    gulp.watch(paths.scriptsES5.src, jsDevES5);
    gulp.watch(paths.styles.src, cssDev);
    gulp.watch(paths.libs.src, libs);
    gulp.watch(paths.libs.src, libsES5);
}
function watchProd() {
    gulp.watch(paths.scripts.src, jsProd);
    gulp.watch(paths.styles.src, cssProd);
    gulp.watch(paths.libs.src, libs);
    gulp.watch(paths.libs.src, libsES5);
}

var buildProd = gulp.series(clean, gulp.parallel(jsProd, jsDevES5, cssProd, libs, libsES5));
var buildDev = gulp.series(clean, gulp.parallel(jsDev, jsDevES5, cssDev, libs, libsES5));

exports.clean = clean;
exports.styles = cssDev;
exports.scripts = jsDev;
exports.libs = libs;
exports.watchDev = watchDev;
exports.watchProd = watchProd;
exports.dev = buildDev;
exports.prod = buildProd;
exports.jsComponents = jsComponents;

exports.default = buildProd;