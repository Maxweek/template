# Template

# JS modules
- modal
- select with search
- header
- side menu
- fix window
- string to date
- word ending
- timers
- masks
- accordeons
- swiper sliders
- anchors
- svg load
- copy inputs

# JS Libraries
- jQuery
- jQuery UI
- jQuery Input mask
- jQuery Cookie

# CDN JS
- swiper
- jQuery

# SCSS modules
- modal
- select
- side menu
- table
- form
- btn
- fonts
- typography
- base
- vars

# BUILDER Gulp
- babel
- uglify
- sass
- concat
- clean
- autoprefixer
- components

# COMMANDS
- dev
- prod
- watch dev